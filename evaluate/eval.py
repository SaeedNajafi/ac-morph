import numpy as np
import codecs
import collections
import sys

def distance(str1, str2):
    """Simple Levenshtein implementation"""
    m = np.zeros([len(str2)+1, len(str1)+1])
    for x in xrange(1, len(str2) + 1):
        m[x][0] = m[x-1][0] + 1
    for y in xrange(1, len(str1) + 1):
        m[0][y] = m[0][y-1] + 1
    for x in xrange(1, len(str2) + 1):
        for y in xrange(1, len(str1) + 1):
            if str1[y-1] == str2[x-1]:
                dg = 0
            else:
                dg = 1
            m[x][y] = min(m[x-1][y] + 1, m[x][y-1] + 1, m[x-1][y-1] + dg)
    return int(m[len(str2)][len(str1)])

pred_file = codecs.open(sys.argv[2], 'r', 'utf-8')
dic = collections.OrderedDict()
for line in pred_file.readlines():
    line = line.strip()
    if len(line)!=0: #ignore newline
        line_lst = line.split('\t')
        try:
            source = line_lst[0]
            target = line_lst[1]
            feature = line_lst[2]
            rank = int(line_lst[3])
            confidence = float(line_lst[4])
            if (source, feature) in dic:
                dic[(source, feature)][0].append(target) #add target to targets list
                dic[(source, feature)][1].append(rank) #add rank to ranks list
                dic[(source, feature)][2].append(confidence)
            else:
                targets = [target] #create target list
                ranks = [rank] #create rank list
                confs = [confidence]
                dic[(source, feature)] = (targets, ranks, confs)
        except:
            print("INFO: Format error in the input file!")

pred_file.close()

new_dic = {}
#sort targets w.r.t. ranks for each source
for (source, feature) in dic:
    (targets, ranks, confs) = dic[(source, feature)]
    sorted_r_t = sorted(zip(confs,targets))
    sorted_targets = list(reversed([t for _,t in sorted_r_t]))
    sorted_confs = list(reversed([r for r,_ in sorted_r_t]))
    new_dic[(source, feature)] = (sorted_targets, sorted_confs)

total = 0.
correct = 0.
edit = 0.
ref_file = codecs.open(sys.argv[1], 'r', 'utf-8')
for line in ref_file.readlines():
    line = line.strip()
    if len(line)==0: continue
    line_arr = line.split("\t")
    source = line_arr[0]
    gold_target = line_arr[1]
    feature = line_arr[2]
    total += 1
    if (source, feature) not in new_dic:
        print("INFO: No prediction for " + source + 'and feature ' + feature)
    else:
        edit += distance(gold_target, new_dic[(source, feature)][0][0])
        if gold_target==new_dic[(source, feature)][0][0]:
            correct += 1

print(str(correct/total) + '\t' + str(edit/total))
