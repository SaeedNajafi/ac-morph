import itertools
import re
import numpy as np
import codecs
from random import shuffle

def load_alphabet_features(cfg):
    #This is where we will keep embeddings data.
    cfg.data = {}

    #Defining some constants.
    cfg.end = u"END"
    cfg.pad = u"PAD"
    cfg.unk = u"UNK"
    cfg.unk_tag = u"TAG"
    cfg.space = u' '

    f = codecs.open(cfg.alphabet, 'r', 'utf-8')
    chars = [line.strip() for line in f.readlines()]
    if cfg.space not in chars: chars.append(cfg.space)
    chars.append(cfg.end)
    chars.append(cfg.pad)
    chars.append(cfg.unk)
    cfg.alphabet_size = len(chars)
    chars.append(cfg.unk_tag)
    f.close()
    f = codecs.open(cfg.feature, 'r', 'utf-8')
    features = [line.strip() for line in f.readlines()]
    f.close()
    chars.extend(features)
    cfg.alphabetANDfeature_size = len(chars)

    id_to_char = dict(enumerate(chars))
    char_to_id = {v:k for k,v in id_to_char.iteritems()}

    cfg.data['id_ch'] = id_to_char
    cfg.data['ch_id'] = char_to_id

    cfg.ch_end_id = cfg.data['ch_id'][cfg.end]
    cfg.ch_pad_id = cfg.data['ch_id'][cfg.pad]
    cfg.ch_unk_id = cfg.data['ch_id'][cfg.unk]
    cfg.ch_unk_tag_id = cfg.data['ch_id'][cfg.unk_tag]
    return
def map_chars_to_ids(cfg, word, features=None, notcopy=False):
    ch_id = cfg.data['ch_id']
    lst = []
    for ch in list(word):
        if ch in ch_id:
            lst.append(ch_id[ch])
        else:
            lst.append(ch_id[cfg.unk])
            print "INFO: Could not find the following char and replaced it with the unk char: ", ch

    if notcopy:
        feat_arr = []
        for f in features.split(";"):
            if f in ch_id:
                feat_arr.append(ch_id[f])
            else:
                feat_arr.append(ch_id[cfg.unk_tag])
                print "INFO: Could not find the following tag and replaced it with the unk tag: ", f

        lst.extend(feat_arr)

    #add end symbol
    lst.append(ch_id[cfg.end])
    return lst
def load_data(cfg):
    """ Loads train, dev or test data. """

    #static batch size
    sb_size = cfg.batch_size

    #local_mode can have three values 'train', 'dev' and 'test'.
    mode = cfg.local_mode

    if mode == 'copy':
        f = cfg.wordlist

    elif mode == 'train':
        f = cfg.train

    elif mode == 'dev':
        f = cfg.dev

    elif mode == 'test':
        f = cfg.test

    batch = []
    fd = codecs.open(f, 'r', 'utf-8')
    lines = fd.readlines()
    if mode=='train' or mode=='copy': shuffle(lines)
    line = None
    for line in lines:
        line = line.strip()
        if len(line)==0: continue
        batch.append(line)
        if len(batch)==sb_size:
            if mode=='train' or mode=='copy': shuffle(batch)
            yield process_batch(cfg, batch)
            batch = []

    fd.close()

    #flush running buffer
    if len(batch)!=0:
        if mode=='train' or mode=='copy': shuffle(batch)
        yield process_batch(cfg, batch)
def process_batch(cfg, batch):
    mode = cfg.local_mode

    hasY = True
    if mode=='test': hasY = False

    Raw_X = []
    X = []
    X_Len = []
    X_Mask = []

    raw_F = []

    Raw_Y = []
    Y = []
    Y_Len = []
    Y_Mask = []

    for line in batch:
        line_arr = line.split("\t")
        if mode=='copy':
            in_x = line_arr[0]
            in_y = in_x
            x = in_x.replace(u' ', cfg.space)
            Raw_X.append(x)
            X_chars = map_chars_to_ids(cfg, x)
            X.append(X_chars)
            X_Len.append(len(X_chars))
            raw_F.append(in_f)
            y = in_y.replace(u' ', cfg.space)
            Raw_Y.append(y)
            Y_chars = map_chars_to_ids(cfg, y)
            Y.append(Y_chars)
            Y_Len.append(len(Y_chars))
        else:
            if hasY:
                in_x = line_arr[0]
                in_y = line_arr[1]
                in_f = line_arr[2]
            else:
                if mode=='dev':
                    in_x = line_arr[0]
                    in_f = line_arr[2]
                
                elif mode=='test':
                    in_x = line_arr[0]
                    in_f = line_arr[1]
            
            x = in_x.replace(u' ', cfg.space)
            Raw_X.append(x)
            X_chars = map_chars_to_ids(cfg, x, in_f, notcopy=True)
            X.append(X_chars)
            X_Len.append(len(X_chars))
            raw_F.append(in_f)
            if hasY:
                y = in_y.replace(u' ', cfg.space)
                Raw_Y.append(y)
                Y_chars = map_chars_to_ids(cfg, y)
                Y.append(Y_chars)
                Y_Len.append(len(Y_chars))

    #Set dynamic batch size
    d_batch_size = len(X_Len)

    #Creating mask for char sequences
    X_Mask = []
    for each in X_Len:
        lst = [1.0] * each
        X_Mask.append(lst)

    if hasY:
        Y_Mask = []
        for each in Y_Len:
            lst = [1.0] * each
            Y_Mask.append(lst)

    #The processed batch is now a dictionary.
    B = {
        'raw_x': Raw_X,
        'x': X,
        'x_mask': X_Mask,
        'raw_f': raw_F,
        'd_batch_size': d_batch_size
        }

    if hasY:
        B['raw_y'] = Raw_Y
        B['y'] = Y
        B['y_mask'] = Y_Mask

    else:
        B['raw_y'] = None
        B['y'] = None
        B['y_mask'] = None

    pad(cfg, B)

    return B
def pad(cfg, B):
    #Pad x with ch_pad_id
    for word in B['x']:
        pad_lst = [cfg.ch_pad_id] * (cfg.max_length-len(word))
        word.extend(pad_lst)

    #Pad x_mask with 0.0
    for word in B['x_mask']:
        pad_lst = [0.0] * (cfg.max_length-len(word))
        word.extend(pad_lst)

    if B['y'] is not None:
        #Pad y with ch_pad_id
        for word in B['y']:
            pad_lst = [cfg.ch_pad_id] * (cfg.max_length-len(word))
            word.extend(pad_lst)

        #Pad y_mask with 0.0
        for word in B['y_mask']:
            pad_lst = [0.0] * (cfg.max_length-len(word))
            word.extend(pad_lst)

    return
