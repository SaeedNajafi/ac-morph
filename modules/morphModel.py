import numpy as np
import torch
from torch.autograd import Variable
import torch.nn as nn
from torch.nn import init

hasCuda = torch.cuda.is_available()

class MorphModel(nn.Module):

    def __init__(self, cfg):
        super(MorphModel, self).__init__()

        self.cfg = cfg

        self.src_rnn = nn.LSTM(
                            input_size=cfg.ch_em_size,
                            hidden_size=cfg.h_units,
                            num_layers=1,
                            bias=True,
                            batch_first=True,
                            dropout=0.0,
                            bidirectional=True
                            )

        self.src_dense_affine = nn.Linear(
                                    2 * cfg.h_units,
                                    cfg.h_units,
                                    bias=True
                                    )

        self.drop = nn.Dropout(cfg.dropout)

        self.dec_rnn = nn.LSTMCell(
                            input_size=cfg.h_units + cfg.ch_em_size,
                            hidden_size=cfg.h_units,
                            bias=True
                            )

        self.atten_W = nn.Parameter(torch.Tensor(cfg.h_units, cfg.h_units), requires_grad=True)
        self.atten_affine = nn.Linear(
                            2 * cfg.h_units,
                            cfg.h_units,
                            bias=True
                            )

        self.dec_affine = nn.Linear(
                            cfg.h_units,
                            cfg.alphabet_size,
                            bias=True
                            )

        self.param_init()
        self.embeddings()
        return

    def param_init(self):
        for name, param in self.named_parameters():
            if 'bias' in name:
                init.constant_(param, 0.0)
            if 'weight' in name:
                init.xavier_uniform_(param)
        return

    def embeddings(self):
        """Add embedding layer that maps from ids to vectors."""
        cfg = self.cfg
        self.ch_em = nn.Embedding(cfg.alphabetANDfeature_size, cfg.ch_em_size)
        self.ch_em.weight.data[cfg.ch_pad_id].fill_(0.0)
        self.ch_em.weight.requires_grad = True

        self.t_ch_em = nn.Embedding(cfg.alphabet_size, cfg.ch_em_size)
        self.t_ch_em.weight.data[cfg.ch_pad_id].fill_(0.0)
        self.t_ch_em.weight.requires_grad = True

        return

    def forward(self, x, x_mask):

        cfg = self.cfg

        ###Encoder
        X_mask = Variable(torch.FloatTensor(x_mask).cuda()) if hasCuda else Variable(torch.FloatTensor(x_mask))
        X = Variable(torch.LongTensor(x).cuda()) if hasCuda else Variable(torch.LongTensor(x))
        zeros = torch.zeros(2, cfg.d_batch_size, cfg.h_units)
        h0 = Variable(zeros.cuda()) if hasCuda else Variable(zeros)
        self.ch_em.weight.data[cfg.ch_pad_id].fill_(0.0)
        X_ems = self.ch_em(X)

        #Bi-directional RNN
        src_outputs, _ = self.src_rnn(X_ems, (h0, h0))
        src_outputs_dr = self.drop(src_outputs)
        HH = torch.tanh(self.src_dense_affine(src_outputs_dr))
        H = HH * X_mask.view(-1, cfg.max_length, 1).expand(-1, cfg.max_length, cfg.h_units)
        ###End of Encoder

        return H

    def train_by_ml(self, H, y, y_mask):
        cfg = self.cfg

        Y = Variable(torch.LongTensor(y).cuda()) if hasCuda else Variable(torch.LongTensor(y))
        Y_mask = Variable(torch.FloatTensor(y_mask).cuda()) if hasCuda else Variable(torch.FloatTensor(y_mask))

        #zero the pad vector
        self.t_ch_em.weight.data[cfg.ch_pad_id].fill_(0.0)
        Y_ems = self.t_ch_em(Y)

        #Create a variable for initial hidden vector of RNN.
        zeros = torch.zeros(cfg.d_batch_size, cfg.h_units)
        h0 = Variable(zeros.cuda()) if hasCuda else Variable(zeros)

        #Create a variable for the initial previous tag.
        zeros = torch.zeros(cfg.d_batch_size, cfg.ch_em_size)
        Go_symbol = Variable(zeros.cuda()) if hasCuda else Variable(zeros)

        #global general attention as https://nlp.stanford.edu/pubs/emnlp15_attn.pdf
        states_mapped = torch.mm(H.view(-1, cfg.h_units), self.atten_W).view(-1, cfg.max_length, cfg.h_units)
        Scores = []
        for i in range(cfg.max_length):
            Hi = H[:,i,:]
            if i==0:
                prev_output = Go_symbol
                h = h0
                c = h0
                context = h0

            input = torch.cat((prev_output, context), dim=1)

            output, c = self.dec_rnn(input, (h, c))

            output_dr = self.drop(output)

            atten_scores = torch.sum(states_mapped * output_dr.view(-1, 1, cfg.h_units).expand(-1, cfg.max_length, cfg.h_units), dim=2)
            atten = nn.functional.softmax(atten_scores, dim=1)
            context = torch.sum(atten.view(-1, cfg.max_length, 1).expand(-1, cfg.max_length, cfg.h_units) * H, dim=1)
            score = self.dec_affine(torch.tanh(self.atten_affine(torch.cat((output_dr, context), dim=1))))

            Scores.append(score)

            #For the next step
            h = output

            gold_prev_output = Y_ems[:,i,:]
            prev_output = gold_prev_output

        #Return log_probs
        return nn.functional.log_softmax(torch.stack(Scores, dim=1), dim=2)

    def loss(self, log_probs, y, y_mask):
        cfg = self.cfg
        y_one_hot = np.zeros((cfg.d_batch_size * cfg.max_length, cfg.alphabet_size))
        y_one_hot[np.arange(cfg.d_batch_size * cfg.max_length), np.reshape(y, (-1,))] = 1.0
        y_o_h = np.reshape(y_one_hot, (cfg.d_batch_size, cfg.max_length, cfg.alphabet_size))

        #ML loss
        Y_mask = Variable(torch.FloatTensor(y_mask).cuda()) if hasCuda else Variable(torch.FloatTensor(y_mask))
        Y_one_hot = Variable(torch.FloatTensor(y_o_h).cuda()) if hasCuda else Variable(torch.FloatTensor(y_o_h))

        objective = torch.sum(Y_one_hot * log_probs, dim=2) * Y_mask
        loss = -1 * torch.mean(torch.mean(objective, dim=1), dim=0)
        return loss
    
    def beam(self, H):
        cfg = self.cfg
        beamsize = cfg.nbest

        #zero the pad vector
        self.t_ch_em.weight.data[cfg.ch_pad_id].fill_(0.0)

        #Create a variable for initial hidden vector of RNN.
        zeros = torch.zeros(cfg.d_batch_size, cfg.h_units)
        h0 = Variable(zeros.cuda()) if hasCuda else Variable(zeros)
        c0 = Variable(zeros.cuda()) if hasCuda else Variable(zeros)

        #Create a variable for the initial previous tag.
        zeros = torch.zeros(cfg.d_batch_size, cfg.ch_em_size)
        Go_symbol = Variable(zeros.cuda()) if hasCuda else Variable(zeros)

        very_negative = torch.zeros(cfg.d_batch_size)
        V_Neg = Variable(very_negative.cuda()) if hasCuda else Variable(very_negative)
        V_Neg.data.fill_(-10**10)

        pads = torch.zeros(cfg.d_batch_size).long()
        Pads = Variable(pads.cuda()) if hasCuda else Variable(pads)
        Pads.data.fill_(cfg.ch_pad_id)

        lprob_candidates = torch.zeros(cfg.d_batch_size, beamsize*beamsize)
        lprob_c = Variable(lprob_candidates.cuda()) if hasCuda else Variable(lprob_candidates)

        hasEnd_candidates = torch.zeros(cfg.d_batch_size, beamsize*beamsize)
        hasEnd_c = Variable(hasEnd_candidates.cuda()) if hasCuda else Variable(hasEnd_candidates)

        y_candidates = torch.zeros(cfg.d_batch_size, beamsize*beamsize).long()
        y_c = Variable(y_candidates.cuda()) if hasCuda else Variable(y_candidates)

        h_candidates = torch.zeros(cfg.d_batch_size, beamsize*beamsize, cfg.h_units)
        h_c = Variable(h_candidates.cuda()) if hasCuda else Variable(h_candidates)

        c_candidates = torch.zeros(cfg.d_batch_size, beamsize*beamsize, cfg.h_units)
        c_c = Variable(c_candidates.cuda()) if hasCuda else Variable(c_candidates)

        context_candidates = torch.zeros(cfg.d_batch_size, beamsize*beamsize, cfg.h_units)
        context_c = Variable(context_candidates.cuda()) if hasCuda else Variable(context_candidates)

        #global general attention as https://nlp.stanford.edu/pubs/emnlp15_attn.pdf
        states_mapped = torch.mm(H.view(-1, cfg.h_units), self.atten_W).view(-1, cfg.max_length, cfg.h_units)
        for i in range(cfg.max_length):
            Hi = H[:,i,:]
            if i==0:
                context = h0
                input = torch.cat((Go_symbol, context), dim=1)
                output, temp_c = self.dec_rnn(input, (h0, c0))

                #atten_scores = torch.sum(H * output.view(-1, 1, cfg.h_units).expand(-1, cfg.max_length, cfg.h_units), dim=2)
                atten_scores = torch.sum(states_mapped * output.view(-1, 1, cfg.h_units).expand(-1, cfg.max_length, cfg.h_units), dim=2)
                atten = nn.functional.softmax(atten_scores, dim=1)
                temp_context = torch.sum(atten.view(-1, cfg.max_length, 1).expand(-1, cfg.max_length, cfg.h_units) * H, dim=1)
                score = self.dec_affine(torch.tanh(self.atten_affine(torch.cat((output, temp_context), dim=1))))

                log_prob = nn.functional.log_softmax(score, dim=1)
                log_prob.data[:, cfg.ch_pad_id] = V_Neg.data #never select pad
                kprob, kidx = torch.topk(log_prob, beamsize, dim=1, largest=True, sorted=True)

                #For the time step > 1
                h = torch.stack([output] * beamsize, dim=1)
                c = torch.stack([temp_c] * beamsize, dim=1)
                context = torch.stack([temp_context] * beamsize, dim=1)
                prev_y = kidx
                prev_lprob = kprob
                hasEnd = kprob - kprob #zeros
                beam = kidx.view(-1, beamsize, 1)

            else:
                beam_candidates = []
                isEnd = torch.eq(prev_y, cfg.ch_end_id).long()
                isEnd_f = isEnd.float()
                prev_output = self.t_ch_em(prev_y)
                for b in range(beamsize):
                    input = torch.cat((prev_output[:,b,:], context[:,b,:]), dim=1)
                    output, temp_c = self.dec_rnn(input, (h[:,b,:], c[:,b,:]))

                    atten_scores = torch.sum(states_mapped * output.view(-1, 1, cfg.h_units).expand(-1, cfg.max_length, cfg.h_units), dim=2)
                    atten = nn.functional.softmax(atten_scores, dim=1)
                    temp_context = torch.sum(atten.view(-1, cfg.max_length, 1).expand(-1, cfg.max_length, cfg.h_units) * H, dim=1)
                    score = self.dec_affine(torch.tanh(self.atten_affine(torch.cat((output, temp_context), dim=1))))

                    log_prob = nn.functional.log_softmax(score, dim=1)
                    log_prob.data[:, cfg.ch_pad_id] = V_Neg.data #never select pad
                    kprob, kidx = torch.topk(log_prob, beamsize, dim=1, largest=True, sorted=True)

                    for bb in range(beamsize):
                        hasEnd_c.data[:,beamsize*b + bb] = torch.gt(hasEnd[:,b] + isEnd_f[:,b], 0).float().data
                        new_lprob = prev_lprob[:,b] + (1.0 - hasEnd_c[:,beamsize*b + bb]) * kprob[:,bb]
                        normalized_new_lprob = torch.div(new_lprob, i)
                        final_new_lprob = isEnd_f[:,b] * normalized_new_lprob + (1.0 - isEnd_f[:,b]) * new_lprob
                        lprob_c.data[:,beamsize*b + bb] = final_new_lprob.data
                        y_c.data[:,beamsize*b + bb] = ((1 - hasEnd_c[:,beamsize*b + bb].long()) * kidx[:,bb] + hasEnd_c[:,beamsize*b + bb].long() * Pads).data
                        h_c.data[:,beamsize*b + bb,:] = output.data
                    	c_c.data[:,beamsize*b + bb,:] = temp_c.data
                    	context_c.data[:,beamsize*b + bb,:] = temp_context.data
                        beam_candidates.append(torch.cat((beam[:,b], y_c[:,beamsize*b + bb].contiguous().view(-1, 1)), 1))

                    for bb in range(1, beamsize):
                        lprob_c.data[:,beamsize*b + bb] = (lprob_c[:,beamsize*b + bb] + hasEnd_c[:,beamsize*b + bb] * V_Neg).data

                formalized_lprob_c = torch.div(lprob_c, i+1)
                _, maxidx = torch.topk(hasEnd_c * lprob_c + (1.0-hasEnd_c) * formalized_lprob_c, beamsize, dim=1, largest=True, sorted=True)

                beam = torch.gather(torch.stack(beam_candidates, dim=1), 1, maxidx.view(-1, beamsize, 1).expand(-1, beamsize, i+1))
                prev_y = torch.gather(y_c, 1, maxidx)
                prev_lprob = torch.gather(lprob_c, 1, maxidx)
                hasEnd = torch.gather(hasEnd_c, 1, maxidx)
                h = torch.gather(h_c, 1, maxidx.view(-1, beamsize, 1).expand(-1, beamsize, cfg.h_units))
                c = torch.gather(c_c, 1, maxidx.view(-1, beamsize, 1).expand(-1, beamsize, cfg.h_units))
                context = torch.gather(context_c, 1, maxidx.view(-1, beamsize, 1).expand(-1, beamsize, cfg.h_units))

        preds = beam
        confidence = prev_lprob
        #confidence is of size (batch size, beam size)
        #preds is of size (batch size, beam size, max length)
        return preds, confidence
