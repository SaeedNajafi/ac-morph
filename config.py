class Configuration(object):
    """Model hyperparams and data information"""
    h_units = 128
    ch_em_size = 64
    dropout = 0.5
    learning_rate = 0.0005
    actor_step_size = 0.5
    max_gradient_norm = 1.
    max_epochs = 512
    max_length = 128
    early_stopping = 25
    batch_size = 8

    #for training with Actor-Critic
    n_step=4

    #nbest results
    nbest=10
