import sys
import codecs

train_file = codecs.open(sys.argv[1], 'r', 'utf-8')
dev_file = codecs.open(sys.argv[2], 'r', 'utf-8')

alphabet_file = codecs.open(sys.argv[3], 'w', 'utf-8')
feature_file = codecs.open(sys.argv[4], 'w', 'utf-8')

alphabet = []
features = []

lines = train_file.readlines()
for line in lines:
	s, t, f = line.strip().split('\t')

	for each_s in list(s):
		if each_s not in alphabet:
			alphabet.append(each_s)

	for each_t in list(t):
		if each_t not in alphabet:
			alphabet.append(each_t)

	for each_f in f.split(";"):
		if each_f not in features:
			features.append(each_f)

train_file.close()

lines = dev_file.readlines()
for line in lines:
	s, t, f = line.strip().split('\t')
	
	for each_s in list(s):
		if each_s not in alphabet:
			alphabet.append(each_s)

	for each_t in list(t):
		if each_t not in alphabet:
			alphabet.append(each_t)

	for each_f in f.split(";"):
		if each_f not in features:
			features.append(each_f)

dev_file.close()

for each in alphabet:
	alphabet_file.write(each + '\n')

for each in features:
	feature_file.write(each + '\n')

alphabet_file.close()
feature_file.close()
