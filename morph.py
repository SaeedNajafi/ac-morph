import argparse
from config import Configuration
from load import load_alphabet_features
from load import load_data
from modules.morphModel import MorphModel
from modules.rltrain import RLTrain
from random import shuffle
from itertools import ifilter
import torch
import torch.optim as optim
import numpy as np
import random
import re
import os
import sys
import time
import codecs

parser = argparse.ArgumentParser()
hasCuda = torch.cuda.is_available()

#Global variables for modules.
mmodel = None
rltrain = None

#Optimizers for modules.
mm_opt = None
rl_opt = None

def save_predictions(cfg, batch, preds, confidence, f):
    """Saves predictions to the provided file stream."""
    nbest = cfg.nbest
    w_idx = 0
    for pred in preds:
        w = batch['raw_x'][w_idx].replace(cfg.space, u' ')
        morph_tag = batch['raw_f'][w_idx]
        for rank in range(nbest):
            end_idx = pred[rank].index(cfg.ch_end_id) if cfg.ch_end_id in pred[rank] else cfg.max_length-1
            target = []
            #do not print end symbol
            for id in range(0, end_idx):
                target.append(cfg.data['id_ch'][pred[rank][id]])

            target_w = ''.join(target).replace(cfg.space, u' ')
            try:
                to_write = w + '\t' + target_w + '\t' + morph_tag + '\t' + str(rank+1) + '\t' + str(confidence[w_idx][rank]) + '\n'
                f.write(to_write)

            except UnicodeDecodeError:
                to_write = u"DUMMY_SOURCE_" + str(w_idx) + '\t' + morph_tag + '\t' + u"DUMMY_TARGET_" + str(rank+1) + '\t' + str(rank+1) + '\t' + str(0.0) + '\n'
                f.write(to_write)
                print "INFO: Found unicode error!"
                pass

        w_idx += 1
    return
def evaluate(cfg, ref_file, pred_file):
    os.system("python %s %s %s > %s" % ('./evaluate/eval.py', ref_file, pred_file, 'temp.score'))
    result_lines = [line.strip() for line in codecs.open('temp.score', 'r', 'utf-8')]
    acc = float(result_lines[0].split('\t')[0])
    distance = float(result_lines[0].split('\t')[1])
    return (1.0 - acc) + distance
def run_ml_epoch(cfg):
    cfg.local_mode = 'train'
    total_loss = []
    #Turn on training mode which enables dropout.
    mmodel.train()
    batches = [batch for batch in load_data(cfg)]
    shuffle(batches)
    steps = len(batches)
    for step in range(steps):
        cfg.d_batch_size = batches[step]['d_batch_size']
        mm_opt.zero_grad()
        H = mmodel(batches[step]['x'], batches[step]['x_mask'])
        log_p = mmodel.train_by_ml(H, batches[step]['y'], batches[step]['y_mask'])
        loss = mmodel.loss(log_p, batches[step]['y'], batches[step]['y_mask'])
        loss.backward()
        torch.nn.utils.clip_grad_norm_(mmodel.parameters(), cfg.max_gradient_norm)
        mm_opt.step()
        loss_value = loss.cpu().data.numpy()
        total_loss.append(loss_value)
        sys.stdout.write('\rBatch:{} | Loss:{} | Mean Loss:{}'.format(
                                                step,
                                                loss_value,
                                                np.mean(total_loss)
                                                )
                            )
        sys.stdout.flush()
    return
def run_copy_epoch(cfg):
    cfg.local_mode = 'copy'
    total_loss = []
    #Turn on training mode which enables dropout.
    mmodel.train()
    batches = [batch for batch in load_data(cfg)]
    shuffle(batches)
    steps = len(batches)
    for step in range(steps):
        cfg.d_batch_size = batches[step]['d_batch_size']
        mm_opt.zero_grad()
        H = mmodel(batches[step]['x'], batches[step]['x_mask'])
        log_p = mmodel.train_by_ml(H, batches[step]['y'], batches[step]['y_mask'])
        loss = mmodel.loss(log_p, batches[step]['y'], batches[step]['y_mask'])
        loss.backward()
        torch.nn.utils.clip_grad_norm_(mmodel.parameters(), cfg.max_gradient_norm)
        mm_opt.step()
        loss_value = loss.cpu().data.numpy()
        total_loss.append(loss_value)
        sys.stdout.write('\rBatch:{} | Loss:{} | Mean Loss:{}'.format(
                                                step,
                                                loss_value,
                                                np.mean(total_loss)
                                                )
                            )
        sys.stdout.flush()
    return
def run_rl_epoch(cfg):
    cfg.local_mode = 'train'
    total_loss = []
    vtotal_loss = []
    #Turn on training mode which enables dropout.
    mmodel.train()
    rltrain.train()
    batches = [batch for batch in load_data(cfg)]
    shuffle(batches)
    steps = len(batches)
    for step in range(steps):
        cfg.d_batch_size = batches[step]['d_batch_size']
        mm_opt.zero_grad()
        rl_opt.zero_grad()
        H = mmodel(batches[step]['x'], batches[step]['x_mask'])
        loss, vloss = rltrain(H, batches[step]['y'], batches[step]['y_mask'], mmodel)
        loss.backward()
        vloss.backward()
        torch.nn.utils.clip_grad_norm_(mmodel.parameters(), cfg.max_gradient_norm)
        mm_opt.step()
        rl_opt.step()
        loss_value = loss.cpu().data.numpy()
        total_loss.append(loss_value)
        vloss_value = vloss.cpu().data.numpy()
        vtotal_loss.append(vloss_value)
        ##
        sys.stdout.write('\rBatch:{} | Loss:{} | Mean Loss:{} | Mean VLoss:{}'.format(
                                                    step,
                                                    loss_value,
                                                    np.mean(total_loss),
                                                    np.mean(vtotal_loss)
                                                    )
                            )
        sys.stdout.flush()
    return
def predict(cfg, out_file):
    if cfg.mode=='train':
        cfg.local_mode = 'dev'

    elif cfg.mode=='test':
        cfg.local_mode = 'test'

    #Turn on evaluation mode which disables dropout.
    mmodel.eval()

    #file stream to save predictions
    f = codecs.open(out_file, 'w','utf-8')
    for batch in load_data(cfg):
        cfg.d_batch_size = batch['d_batch_size']
        H = mmodel(batch['x'], batch['x_mask'])
        p, c = mmodel.beam(H)
        preds = p.cpu().data.numpy()
        confidence = np.exp(c.cpu().data.numpy())
        save_predictions(cfg, batch, preds.tolist(), confidence.tolist(), f)

    f.close()
    return
def run_model(
        mode,
        model,
        path,
        seed,
        alphabet_file,
        feature_file,
        train_file=None,
        dev_file=None,
        word_list=None,
        test_file=None,
        out_file=None):

    global mmodel, rltrain, mm_opt, rl_opt

    cfg = Configuration()

    cfg.mode = mode
    cfg.seed = int(seed)
    cfg.alphabet = alphabet_file
    cfg.feature = feature_file
    cfg.train = train_file
    cfg.dev = dev_file
    cfg.wordlist = word_list
    cfg.test = test_file
    cfg.model_type = model

    #Set Random Seeds
    random.seed(cfg.seed)
    np.random.seed(cfg.seed)
    torch.manual_seed(cfg.seed)
    if hasCuda:
        torch.cuda.manual_seed_all(cfg.seed)

    load_alphabet_features(cfg)

    #Construct models
    mmodel = MorphModel(cfg)
    if hasCuda: mmodel.cuda()

    if mode=='train':
        mm_opt = optim.Adam(ifilter(lambda p: p.requires_grad, mmodel.parameters()), lr=cfg.learning_rate)
        #ML copy phase with external wordlists
        epoch=0
        temp = cfg.batch_size
        cfg.batch_size = 256
        while (epoch < 50):
            print
            print 'Copy Epoch:{}'.format(epoch)
            run_copy_epoch(cfg)

        cfg.batch_size = temp
        torch.save(mmodel.state_dict(), path + 'morphModel')

        print "INFO: Finished copy phase!"

        #ML supervised phase
        out_file = './temp.predicted'
        best_val_cost = float('inf')
        best_val_epoch = 0
        first_start = time.time()
        epoch=0
        cfg.nbest = 1
        while (epoch < cfg.max_epochs):
            print
            print 'Supervised ML Epoch:{}'.format(epoch)

            start = time.time()
            run_ml_epoch(cfg)
            print '\nValidation:'
            temp = cfg.batch_size
            #speed up prediciton
            cfg.batch_size = 512
            predict(cfg, out_file)
            cfg.batch_size = temp
            val_cost = evaluate(cfg, cfg.dev, out_file)
            print 'Validation cost:{}'.format(val_cost)
            if val_cost < best_val_cost:
                best_val_cost = val_cost
                best_val_epoch = epoch
                torch.save(mmodel.state_dict(), path + 'morphModel')

            #For early stopping
            if epoch - best_val_epoch > cfg.early_stopping:
                break
                ###

            print 'Epoch training time:{} seconds'.format(time.time() - start)
            epoch += 1

        print 'Total training time:{} seconds'.format(time.time() - first_start)

        print "INFO: Finished ML phase!"

        #RL phase
        mm_opt = optim.SGD(ifilter(lambda p: p.requires_grad, mmodel.parameters()), lr=cfg.actor_step_size)
        rltrain = RLTrain(cfg)
        if hasCuda:
            rltrain.cuda()
        rl_opt = optim.Adam(ifilter(lambda p: p.requires_grad, rltrain.parameters()), lr=cfg.learning_rate, weight_decay=0.001)
        out_file = './temp.predicted'
        best_val_cost = float('inf')
        best_val_epoch = 0
        first_start = time.time()
        epoch=0
        cfg.nbest = 1
        while (epoch < cfg.max_epochs):
            print
            print 'RL Epoch:{}'.format(epoch)

            start = time.time()
            run_rl_epoch(cfg)
            print '\nValidation:'
            temp = cfg.batch_size
            #speed up prediciton
            cfg.batch_size = 512
            predict(cfg, out_file)
            cfg.batch_size = temp
            val_cost = evaluate(cfg, cfg.dev, out_file)
            print 'Validation cost:{}'.format(val_cost)
            if val_cost < best_val_cost:
                best_val_cost = val_cost
                best_val_epoch = epoch
                torch.save(mmodel.state_dict(), path + 'morphModel')

            #For early stopping
            if epoch - best_val_epoch > cfg.early_stopping:
                break
                ###

            print 'Epoch training time:{} seconds'.format(time.time() - start)
            epoch += 1

        print 'Total training time:{} seconds'.format(time.time() - first_start)

        print "INFO: Finished RL phase!"

    elif mode=='test':
        temp = cfg.batch_size
        #speed up prediction
        cfg.batch_size = 512
        mmodel.load_state_dict(torch.load(path + 'morphModel'))
        print
        print 'Predicting'
        start = time.time()
        predict(cfg, out_file)
        print 'Total prediction time:{} seconds'.format(time.time() - start)
        cfg.batch_size = temp
    return
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--mode', type=str, required=True, default='train', help='train | test')
    parser.add_argument('--path', type=str, required=True, default='./', help='Path for saving or loading model')
    parser.add_argument('--seed', type=int, default=11, help='Integer random seed')
    parser.add_argument('--alphabet_file', type=str, required=True, default='alph.txt', help='File containig alphabet')
    parser.add_argument('--feature_file', type=str, required=True, default='feat.txt', help='File containig features')

    #Train specific
    parser.add_argument('--train_file', type=str, default='train.txt', help='Path for train data')
    parser.add_argument('--dev_file', type=str, default='dev.txt', help='Path for dev data')
    parser.add_argument('--word_list', type=str, default='wordlist.txt', help='Path for wordlist')

    #Test specific
    parser.add_argument('--test_file', type=str, default='test.txt', help='Path for test data')
    parser.add_argument('--out_file', type=str, default='predictions.txt', help='Path for saving predictions')

    args = parser.parse_args()
    run_model(
            args.mode,
            args.path,
            args.seed,
            args.alphabet_file,
            args.feature_file,
            args.train_file,
            args.dev_file,
            args.word_list,
            args.test_file,
            args.out_file)
